# CALIBRE LIBRARY

CALIB (CALIBRE LIBRARY) is the collection of manuals, books, guides and documents for revolutionary & insurrectionary self-defense

## LICENSE - [GNU Affero General Public License v3.0](https://gitlab.com/calibrelib/calib/-/blob/master/LICENSE)

## TO DOWNLOAD

    git clone https://gitlab.com/calibrelib/calib.git
    
https://gitlab.com/calibrelib/calib/-/archive/master/calib-master.tar.gz
