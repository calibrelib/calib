@charset "UTF-8";

/* --- GoToTop_purecss.css --- */



.SedoGoToTop {
	font-size: 27px;
color: rgb(47, 68, 86);
text-decoration: none;
background-color: rgb(237, 237, 237);
border-radius: 10px;
display: none;
position: fixed;
overflow: hidden;
cursor: pointer;
box-shadow: 0 0 3px 1px rgb(203, 203, 203);
opacity: 0.92;
z-index: 90;
width: 53px;
height: 52px;

	height: auto;
	bottom: 10px;
	right: 10px;	
}



.SedoGoToTop.mini {
	font-size: 13px;
border-radius: 5px;
width: 27px;
height: 26px;

	height: auto;	
}

.SedoGoToTop:active, #toTop:focus {
	outline:none;
}

.SedoGoToTop #toTopHover {
	color: #176093;
background-color: rgb(47, 68, 86);
display: block;
position: absolute;
overflow: hidden;
float: left;
opacity: 0;

	filter: alpha(opacity=0);
	width: 53px;
	height: 100%;
	background-color: rgb(237, 237, 237)
}

.SedoGoToTop.mini #toTopHover {
	width: 27px;
}

.SedoGoToTop ul {
	display: inline-block;
	text-align: center;
	vertical-align: middle;
	width: 100%;
	height: 100%;
}

.SedoGoToTop ul li {
	padding-bottom: 2px;
	-moz-user-select: -moz-none;
	-khtml-user-select: none;
	-webkit-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

.SedoGoToTop ul li:first-child,
.SedoGoToTop ul li.AdvQm {
	border-bottom: 1px solid rgb(47, 68, 86);
}

.SedoGoToTop #toTopHover ul li:first-child,
.SedoGoToTop #toTopHover ul li.AdvQm {
	border-bottom: 1px solid #176093;
}

.SedoGoToTop li{
	height: 27px;
	line-height: 27px;
}

.SedoGoToTop.mini li{
	height: 14px;
	line-height: 14px;
}

.SedoGoToTop ul li:hover {
	background-color: rgb(237, 237, 237);

	background-color: rgb(47, 68, 86)	
}

.SedoGoToTop p.solo {
	width: 100%;
	height: 100%;
	text-align: center;
	height: 52px;
	line-height: 52px;
}

.SedoGoToTop.mini p.solo {
	height: 26px;
	line-height: 26px;
}


.SedoGoToTop ul li.AdvQm {
	font-style: italic;
font-size: 15px;

}


.SedoGoToTop.mini ul li.AdvQm {
	font-size: 9px;

}

.SedoGoToTop ul li.AdvQm.active {
	background-color: rgb(255, 255, 200);

}

/* --- bb_code.css --- */

.quoteContainer:not(.expanded) [data-s9e-mediaembed] iframe
	{
		position: unset !important;
	}.quoteContainer.expanded iframe[data-s9e-mediaembed],
	.quoteContainer.expanded [data-s9e-mediaembed] iframe
	{
		max-height: none;
		max-width:  none;
	}/* .bbCodeX classes are designed to exist inside .baseHtml. ie: they have no CSS reset applied */

.bbCodeBlock
{
	margin: 1em 140px 1em 0;
border: 1px solid rgb(237, 237, 237);
border-radius: 5px;
overflow: auto;

}

	.bbCodeBlock .bbCodeBlock,
	.hasJs .bbCodeBlock .bbCodeSpoilerText,
	.messageList.withSidebar .bbCodeBlock
	{
		margin-right: 0;
	}

	/* mini CSS reset */
	.bbCodeBlock pre,
	.bbCodeBlock blockquote
	{
		margin: 0;
	}
	
	.bbCodeBlock img
	{
		border: none;
	}

.bbCodeBlock .type
{
	font-size: 11px;
font-family: 'Trebuchet MS', Helvetica, Arial, sans-serif;
color: #6cb2e4;
background: rgb(237, 237, 237) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x top;
padding: 3px 8px;
border-bottom: 1px solid rgb(203, 203, 203);
border-top-left-radius: 4px;
border-top-right-radius: 4px;

}

.bbCodeBlock pre,
.bbCodeBlock .code
{
	font-size: 10pt;
font-family: Consolas, 'Courier New', Courier, monospace;
background: rgb(245, 245, 245) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
padding: 10px;
border-radius: 5px;
word-wrap: normal;
overflow: auto;
line-height: 1.24;
min-height: 30px;
max-height: 500px;
_width: 600px;
direction: ltr;

}

.bbCodeBlock .code
{
	white-space: nowrap;
}

.bbCodeQuote
{
	border-color: #f9d9b0;
overflow: auto;

}

.bbCodeQuote .attribution
{
	color: rgb(20,20,20);
background: #f9d9b0 url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x top;
border-bottom: 1px solid #f9bc6d;

}

.bbCodeQuote .quoteContainer
{
	overflow: hidden;
	position: relative;
	
	font-style: italic;
font-size: 9pt;
background: #fff4e5 url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
padding: 10px;
border-radius: 4px;

}


	.bbCodeQuote .quoteContainer .quote
	{
		max-height: 150px;
		overflow: hidden;
		padding-bottom: 1px;
	}
	
		.NoJs .bbCodeQuote .quoteContainer .quote
		{
			max-height: none;
		}

	.bbCodeQuote .quoteContainer .quoteExpand
	{		
		display: none;
		box-sizing: border-box;
		position: absolute;
		height: 80px;
		top: 90px;
		left: 0;
		right: 0;
		
		font-size: 11px;
		line-height: 1;
		text-align: center;
		color: #f9bc6d;
		cursor: pointer;
		padding-top: 65px;
		background: -webkit-linear-gradient(top, rgba(255, 244, 229, 0) 0%, #fff4e5 80%);
		background: -moz-linear-gradient(top, rgba(255, 244, 229, 0) 0%, #fff4e5 80%);
		background: -o-linear-gradient(top, rgba(255, 244, 229, 0) 0%, #fff4e5 80%);
		background: linear-gradient(to bottom, rgba(255, 244, 229, 0) 0%, #fff4e5 80%);
		
		border-bottom-left-radius: 4px;
		border-bottom-right-radius: 4px;
	}
	
	.bbCodeQuote .quoteContainer .quoteExpand.quoteCut
	{
		display: block;
	}
	
	.bbCodeQuote .quoteContainer.expanded .quote
	{
		max-height: none;
	}
	
	.bbCodeQuote .quoteContainer.expanded .quoteExpand
	{
		display: none;
	}


	.bbCodeQuote img
	{
		max-height: 150px;
	}
	
	.bbCodeQuote iframe, .bbCodeQuote [data-s9e-mediaembed],
	.bbCodeQuote .fb_iframe_widget,
	.bbCodeQuote object,
	.bbCodeQuote embed
	{
		max-width: 200px;
		max-height: 150px;
	}
	
	.bbCodeQuote iframe:-webkit-full-screen
	{
		max-width: none;
		max-height: none;
	}
	
	.bbCodeQuote iframe:-moz-full-screen
	{
		max-width: none;
		max-height: none;
	}
	
	.bbCodeQuote iframe:-ms-fullscreen
	{
		max-width: none;
		max-height: none;
	}
	
	.bbCodeQuote iframe:fullscreen
	{
		max-width: none;
		max-height: none;
	}
	
.bbCodeSpoilerButton
{
	margin: 5px 0;
	max-width: 99%;
}

	.bbCodeSpoilerButton > span
	{
		display: inline-block;
		max-width: 100%;
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
	
.hasJs .bbCodeSpoilerText
{
	display: none;
	background-color: rgb(245, 245, 245);
padding: 5px;
margin-top: 5px;
margin-right: 140px;
margin-bottom: 5px;
border: 1px solid rgb(237, 237, 237);
border-radius: 5px;
overflow: auto;

}

	.hasJs .bbCodeSpoilerText .bbCodeSpoilerText,
	.hasJs .bbCodeSpoilerText .bbCodeBlock,
	.hasJs .messageList.withSidebar .bbCodeSpoilerText
	{
		margin-right: 0;
	}
	
.NoJs .bbCodeSpoilerContainer
{
	background-color: rgb(20,20,20); /* fallback for browsers without currentColor */
	background-color: currentColor;
}

	.NoJs .bbCodeSpoilerContainer > .bbCodeSpoilerText
	{
		visibility: hidden;
	}

	.NoJs .bbCodeSpoilerContainer:hover
	{
		background-color: transparent;
	}
	
		.NoJs .bbCodeSpoilerContainer:hover > .bbCodeSpoilerText
		{
			visibility: visible;
		}


@media (max-width:800px)
{
	.Responsive .bbCodeBlock,
	.Responsive.hasJs .bbCodeSpoilerText
	{
		margin-right: 0;
	}
}


/* --- cta_featuredthreads.css --- */

.ctaFtTitleForumList
{
	font-weight: bold;
margin: 10px auto -5px;

}

.ctaFtTitleForumView
{
	font-weight: bold;
margin: 10px auto -5px;

}

.ctaFtTitleNewPosts
{
	font-weight: bold;
margin: 10px auto -5px;

}

.ctaFtFeaturedThreadList
{
	font-size: 10px;
color: #032A46;
background-color: rgb(237, 237, 237);
padding: 1px 2px;
margin-left: 5px;
border: 1px solid rgb(203, 203, 203);
border-radius: 3px;
float: right;

}


	.ctaFtFeaturedThreadList:hover,
	.ctaFtFeaturedThreadList a:hover
	{
		text-decoration: none;
background-color: rgb(203, 203, 203);
border-color: #6cb2e4;

	}


.ctaFtFeaturedThreadView
{
	font-size: 18px;
color: #032A46;
background: rgb(203, 203, 203) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x top;
padding: 0px 6px;
margin: 4px 0px 0px 5px;
border: 1px solid #6cb2e4;
border-radius: 4px;
float: right;
clear: right;
display: inline-block;

}


	.ctaFtFeaturedThreadView:hover,
	.ctaFtFeaturedThreadView a:hover
	{
		text-decoration: none;
background-color: #6cb2e4;
border-color: rgb(87, 120, 142);

	}


.ctaFtExpiredThreadList
{
	font-size: 10px;
color: rgb(100,100,100);
background-color: #fff4e5;
padding: 1px 2px;
margin-left: 5px;
border: 1px solid #f9d9b0;
border-radius: 3px;
float: right;

}

.ctaFtExpiredThreadView
{
	font-size: 18px;
color: rgb(100,100,100);
background: #fff4e5 url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x top;
padding: 0px 6px;
margin: 4px 0px 0px 5px;
border: 1px solid #f9d9b0;
border-radius: 4px;
float: right;
clear: right;
display: inline-block;

}

.ctaFtBlock
{
	margin-top: 10px;

}

	.ctaFtBlock .prefix
	{
		font-size: 80%;
		font-weight: normal;
		line-height: 18px;
		margin: 0;
	}

.sectionMain.ctaFtContainer
{
	margin-top: 0;
margin-bottom: 10px;
border-color: rgb(203, 203, 203);

}

.ctaFtBackground
{
	background: rgb(245, 245, 245) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/images/cta-featured-threads-gradient.png') repeat-x top;

}

.ctaFtBackgroundImage
{
	background-repeat: no-repeat !important;
background-position: center center !important;
background-size: cover !important;

}

.ctaFtAvatar
{
	margin-right: 10px;
float: left;
width: 102px;
height: 102px;

}

.ctaFtAvatarRight
{
	float: right;
	margin-right: 0;
	margin-left: 10px;
}

.ctaFtIcon
{
	background-color: rgb(220, 220, 220);
padding: 2px;
margin-right: 10px;
border: 1px solid rgb(203, 203, 203);
border-radius: 4px;
float: left;
width: 96px;
height: 96px;

}

	.ctaFtIcon img
	{
		width: 96px;
		height: 96px;
	}

.ctaFtIconRight
{
	float: right;
	margin-right: 0;
	margin-left: 10px;
}

.ctaFtThreadContentAvatar
{
	height: 102px;

}

.ctaFtThreadContentIcon
{
	height: 102px;

}

.ctaFtThreadTitle
{
	font-weight: bold;
font-size: 18px;
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
word-wrap: normal;

}

a.ctaFtThreadTitleLink
{
	
}

.ctaFtThreadTextAvatar
{
	font-size: 14px;
overflow: hidden;
line-height: 1.44em;
height: 78px;

}

.ctaFtThreadTextIcon
{
	font-size: 14px;
overflow: hidden;
line-height: 1.44em;
height: 78px;

}

	.ctaFtThreadTextAvatar a,
	.ctaFtThreadTextIcon a
	{
		
	}

.ctaFtFooter
{
	padding-top: 1px;
margin-bottom: -3px;
overflow: hidden;

}

/* clearfix */ .ctaFtFooter { zoom: 1; } .ctaFtFooter:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

.ctaFtFeaturedThreadText
{
	font-weight: bold;
font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 20px;
float: left;

}

.ctaFtAuthor .username
{
	font-weight: bold;
font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;
max-width: 100px;
display: inline-block;
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
word-wrap: normal;

}

.ctaFtDate
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;

}

.ctaFtForum a
{
	font-weight: bold;
font-size: 12px;
margin-right: 10px;
float: left;

}

.ctaFtExpiry
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;

}

.ctaFtReplies
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

.ctaFtLikes
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

.ctaFtViews
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

a.ctaFtReadMoreLink
{
	font-weight: bold;
font-size: 12px;
margin-left: 10px;
float: right;

}

.ctaFtShare
{
	margin-top: 10px;
margin-bottom: -8px;

}

/* clearfix */ .ctaFtShare { zoom: 1; } .ctaFtShare:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

.ctaFtShareControls
{
	padding-bottom: 5px;
margin-right: 10px;
float: left;

}

.ctaFtTwitter
{
	
}

.ctaFtGoogle
{
	
}

.ctaFtFacebook
{
	
}

.ctaFtAddThis
{
	
}

.ctaFtCustomShare
{
	
}

.ctaFtListItemsPage
{
	
}

.ctaFtBlockPage
{
	margin-top: 10px;

}

	.ctaFtBlockPage .prefix
	{
		font-size: 80%;
		font-weight: normal;
		line-height: 18px;
		margin: 0;
	}

.sectionMain.ctaFtContainerPage
{
	margin-top: 0;
margin-bottom: 10px;
border-color: rgb(203, 203, 203);

}

.ctaFtBackgroundPage
{
	background: rgb(245, 245, 245) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/images/cta-featured-threads-gradient.png') repeat-x top;

}

.ctaFtBackgroundImagePage
{
	background-position: center center !important;
background-size: cover !important;

}

.ctaFtAvatarPage
{
	margin-right: 10px;
float: left;
width: 102px;
height: 102px;

}

.ctaFtAvatarRightPage
{
	float: right;
	margin-right: 0;
	margin-left: 10px;
}

.ctaFtIconPage
{
	background-color: rgb(220, 220, 220);
padding: 2px;
margin-right: 10px;
border: 1px solid rgb(203, 203, 203);
border-radius: 4px;
float: left;
width: 192px;
height: 192px;

}

	.ctaFtIconPage img
	{
		width: 192px;
		height: 192px;
	}

.ctaFtIconRightPage
{
	float: right;
	margin-right: 0;
	margin-left: 10px;
}

.ctaFtThreadContentAvatarPage
{
	min-height: 102px;

}

.ctaFtThreadContentIconPage
{
	min-height: 198px;

}

.ctaFtThreadTitlePage
{
	font-weight: bold;
font-size: 18px;
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
word-wrap: normal;

}

a.ctaFtThreadTitleLinkPage
{
	
}

.ctaFtThreadTextAvatarPage
{
	font-size: 14px;
line-height: 1.44em;

}

.ctaFtThreadTextIconPage
{
	font-size: 14px;
line-height: 1.44em;

}

	.ctaFtThreadTextAvatarPage a,
	.ctaFtThreadTextIconPage a
	{
		
	}

.ctaFtFooterPage
{
	padding-top: 1px;
margin-bottom: -3px;
overflow: hidden;

}

/* clearfix */ .ctaFtFooterPage { zoom: 1; } .ctaFtFooterPage:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

.ctaFtFeaturedThreadTextPage
{
	font-weight: bold;
font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 20px;
float: left;

}

.ctaFtAuthorPage .username
{
	font-weight: bold;
font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;
max-width: 100px;
display: inline-block;
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
word-wrap: normal;

}

.ctaFtDatePage
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;

}

.ctaFtForumPage a
{
	font-weight: bold;
font-size: 12px;
margin-right: 10px;
float: left;

}

.ctaFtExpiryPage
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;

}

.ctaFtRepliesPage
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

.ctaFtLikesPage
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

.ctaFtViewsPage
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

a.ctaFtReadMoreLinkPage
{
	font-weight: bold;
font-size: 12px;
margin-left: 10px;
float: right;

}

.ctaFtSharePage
{
	margin-top: 10px;
margin-bottom: -8px;

}

/* clearfix */ .ctaFtSharePage { zoom: 1; } .ctaFtSharePage:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

.ctaFtShareControlsPage
{
	padding-bottom: 5px;
margin-right: 10px;
float: left;

}

.ctaFtTwitterPage
{
	
}

.ctaFtGooglePage
{
	
}

.ctaFtFacebookPage
{
	
}

.ctaFtAddThisPage
{
	
}

.ctaFtCustomSharePage
{
	
}

.ctaFtInfoTextPage
{
	
}

.footerLinks a.ctaFtRssFeedPage
{
	
}

.ctaFtListItemsArchive
{
	
}

.ctaFtBlockArchive
{
	margin-top: 10px;

}

	.ctaFtBlockArchive .prefix
	{
		font-size: 80%;
		font-weight: normal;
		line-height: 18px;
		margin: 0;
	}

.ctaFtContainerArchive
{
	margin-top: 0;
margin-bottom: 10px;
border-color: rgb(203, 203, 203);

}

.ctaFtArchiveDeleted
{
	opacity: 0.5;
}

.ctaFtArchiveDeletedInfo
{
	margin: 10px 10px 0;
	color: rgb(150, 0, 0);
	font-size: 12px;
}

.ctaFtBackgroundArchive
{
	background: rgb(245, 245, 245) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/images/cta-featured-threads-gradient.png') repeat-x top;

}

.ctaFtAvatarArchive
{
	margin-right: 10px;
float: left;
width: 102px;
height: 102px;

}

.ctaFtThreadContentArchive
{
	height: 102px;

}

.ctaFtThreadTitleArchive
{
	font-weight: bold;
font-size: 18px;
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
word-wrap: normal;

}

a.ctaFtThreadTitleLinkArchive
{
	
}

.ctaFtThreadTextArchive
{
	font-size: 14px;
overflow: hidden;
line-height: 1.44em;
height: 78px;

}

.ctaFtThreadTextArchive a
{
	
}

.ctaFtFooterArchive
{
	padding-top: 1px;
margin-bottom: -3px;
overflow: hidden;

}

/* clearfix */ .ctaFtFooterArchive { zoom: 1; } .ctaFtFooterArchive:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

.ctaFtAuthorArchive .username
{
	font-weight: bold;
font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;
display: inline-block;
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
word-wrap: normal;
width: 102px;

}

.ctaFtDateArchive
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-right: 10px;
float: left;

}

.ctaFtForumArchive a
{
	font-weight: bold;
font-size: 12px;
margin-right: 10px;
float: left;

}

.ctaFtRepliesArchive
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

.ctaFtLikesArchive
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

.ctaFtViewsArchive
{
	font-size: 12px;
color: rgb(100, 100, 100);
margin-left: 10px;
float: right;

}

a.ctaFtReadMoreLinkArchive
{
	font-weight: bold;
font-size: 12px;
margin-left: 10px;
float: right;

}

.ctaFtInfoTextArchive
{
	
}

.footerLinks a.ctaFtRssFeedArchive
{
	
}

.ctaFtAuthorFeaturedThreads
{
	
}

.ctaFtAuthorList
{
	
}

.ctaFtAuthorListItem
{
	border-bottom: 1px solid rgb(237, 237, 237);
	overflow: hidden;
	padding: 0 0 10px;
}

	.member_view .ctaFtAuthorListItem:first-child
	{
		margin-top: -5px;
	}

	.ctaFtAuthorListItem .avatar
	{
		float: left;
		margin: 5px 0;
	}

	.ctaFtAuthorListItem .prefix
	{
		font-size: 80%;
		font-weight: normal;
		line-height: 16px;
		margin: 0;
	}

.ctaFtAuthorAvatar
{
	
}

.ctaFtAuthorDeleted
{
	opacity: 0.5;
}

.ctaFtAuthorDeletedInfo
{
	margin-left: 10px;
	color: rgb(150, 0, 0);
	font-size: 12px;
}

.ctaFtAuthorContent
{
	margin-left: 56px;
	padding: 5px;
}

.ctaFtAuthorTitleText
{
    margin-bottom: 2px;
    overflow: hidden;
}

.ctaFtAuthorTitle
{
	font-size: 11pt;
	line-height: 18px;
}

.ctaFtAuthorSnippet
{
	margin-bottom: 2px;
}

	.ctaFtAuthorListItem .ctaFtAuthorSnippet a
	{
		font-size: 11px;
		color: rgb(20,20,20);
		text-decoration: none;
	}

.ctaFtAuthorMeta
{
    color: rgb(100, 100, 100);
    font-size: 11px;
    margin-bottom: 2px;
    overflow: hidden;
}

	.ctaFtAuthorMeta a
	{
		color: rgb(100, 100, 100);
	}

.ctaFtInfoTextAuthor
{
	
}

.ctaFtImageEditor
{
	overflow: hidden;
	zoom: 1;
	margin: 0 auto;
}

.ctaFtImageEditor #CtaFtImagePanes
{
	margin-top: 10px;
}

.ctaFtIconEditor
{
	
}

.ctaFtBackgroundEditor
{
	
}

.ctaFtSliderEditor
{
	
}

.ctaFtImageEditor .ctaFtModifyControls
{
	margin-left: 112px;
	min-height: 102px;
}

.ctaFtImageEditor .ctaFtModifyControlsLarge
{
	margin-left: 208px;
	min-height: 198px;
}

.ctaFtImageEditor .ctaFtModifyControlsNoImage
{
	min-height: 102px;
}

.ctaFtImageEditor .faint
{
	font-size: 11px;
}

.ctaFtImageEditor .submitUnit
{
	text-align: right;
}

.ctaFtImageEditor .ctaFtCurrentImage
{
	background-color: transparent;
	padding: 2px;
	border: 1px solid rgb(203, 203, 203);
	border-radius: 4px;
	float: left;
	display: block;
	width: 192px;
	text-align: center;
}

.ctaFtImageEditor .ctaFtCurrentImageAvatar
{
	padding: 0;
	border: 0;
	border-radius: 0;
	width: 102px;
}

	.ctaFtImageEditor .ctaFtCurrentImage img
	{
		display: block;
		max-width: 96px;
		max-height: 96px;
		height: auto;
		margin: 0 auto;
	}

.ctaFtImageEditor .ctaFtCurrentImageLarge
{
	background-color: transparent;
	padding: 2px;
	border: 1px solid rgb(203, 203, 203);
	border-radius: 4px;
	float: left;
	display: block;
	width: 192px;
	text-align: center;
}

	.ctaFtImageEditor .ctaFtCurrentImageLarge img
	{
		display: block;
		max-width: 192px;
		max-height: 192px;
		height: auto;
		margin: 0 auto;
	}

.ctaFtImageEditor .ctaFtImageAction
{
	overflow: hidden; zoom: 1;
	padding: 10px;
	border: 1px solid #176093;
	border-radius: 5px;
	margin-bottom: 10px;
}

.xenOverlay .ctaFtImageEditor .ctaFtImageAction
{
	background: url(/web/20190727050457im_/https://www.northeastshooters.com/xen/rgba.php?r=0&g=0&b=0&a=63); background: rgba(0,0,0, 0.25); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#3F000000,endColorstr=#3F000000);
}

.ctaFtImageEditor .ctaFtImageUpload
{
	max-width: 100%;
	box-sizing: border-box;
	margin: 3px 0;
}

.ctaFtImageEditor .ctaFtImageUploadUrl
{
	width: 100%;
	box-sizing: border-box;
	margin: 3px 0;
}

.ctaFtImageEditor .ctaFtIcon
{
	border: 0;
	border-radius: 0;
	padding: 0;
}

.ctaFtImageEditor .ctaFtIconLarge
{
	border: 0;
	border-radius: 0;
	padding: 0;
	width: 192px;
}

.ctaFtImageEditor .ctaFtBackground
{
	background: none;
	border: 0;
	border-radius: 0;
	padding: 0;
}

.ctaFtImageEditor .ctaFtSlider
{
	border: 0;
	border-radius: 0;
	padding: 0;
}



.ctaFtImageEditor .ctaFtAttachedImages
{
	margin-top: 3px;
	max-height: 106px;
	height: auto;
	overflow-y: auto;
}

.ctaFtImageEditor .ctaFtAttachedImages img
{
	display: block;
	max-width: 100px;
	max-height: 100px;
	cursor: pointer;
}

.ctaFtImageEditor .ctaFtAttachedImage
{
	position: relative;
	display: inline-block;
	background-color: transparent;
	padding: 2px;
	border: 1px solid rgb(203, 203, 203);
	border-radius: 4px;
	width: 100px;
	max-height: 100px;
	float: left;
	margin: 10px 4px 0 0;
	
}

.ctaFtImageEditor .ctaFtAttachedImageInput
{
	position: absolute;
	top: 4px;
	left: 4px
}

.sectionMain.ctaFtContainer .Popup .arrowWidget,
.sectionMain.ctaFtContainerPage .Popup .arrowWidget
{
	width: 15px;
}

.ctaFtBlock .Popup .arrowWidget,
.ctaFtBlockPage .Popup .arrowWidget
{
	width: 15px;
}






.ctaFtThreadTextAvatar img.mceSmilie,
.ctaFtThreadTextAvatar img.mceSmilieSprite,
.ctaFtThreadTextIcon img.mceSmilie,
.ctaFtThreadTextIcon img.mceSmilieSprite
{
	display: none;
}









@media (max-width:610px)
{
	.Responsive .footerLinks a.ctaFtRssFeedPage,
	.Responsive .footerLinks a.ctaFtRssFeedArchive
	{
		display: none;
	}
}

@media (max-width:480px)
{
	.Responsive .xenForm .ctrlUnit > dd,
	.Responsive .xenForm .ctrlUnit.fullWidth dd
	{
		padding-left: 0px;
		padding-right: 0px;
	}

	.ctaThreadListItemControls
	{
		display: none;
	}
}


.ctaFtThreadTextAvatar.baseHtml ol,
.ctaFtThreadTextAvatar.baseHtml ul,
.ctaFtThreadTextAvatar.baseHtml dl
{
	margin-left: 0;
}

.ctaFtThreadTextAvatar.baseHtml li
{
	position: relative;
	left: 2em;
	margin-right: 2em;
}

.ctaFtThreadTextIcon.baseHtml ol,
.ctaFtThreadTextIcon.baseHtml ul,
.ctaFtThreadTextIcon.baseHtml dl
{
	margin-left: 0;
}

.ctaFtThreadTextIcon.baseHtml li
{
	position: relative;
	left: 2em;
	margin-right: 2em;
}

.ctaFtThreadTextAvatarPage.baseHtml ol,
.ctaFtThreadTextAvatarPage.baseHtml ul,
.ctaFtThreadTextAvatarPage.baseHtml dl
{
	margin-left: 0
}

.ctaFtThreadTextAvatarPage.baseHtml li
{
	position: relative;
	left: 2em;
	margin-right: 2em;
}

.ctaFtThreadTextIconPage.baseHtml ol,
.ctaFtThreadTextIconPage.baseHtml ul,
.ctaFtThreadTextIconPage.baseHtml dl
{
	margin-left: 0;
}

.ctaFtThreadTextIconPage.baseHtml li
{
	position: relative;
	left: 2em;
	margin-right: 2em;
}

.ctaFtThreadTextAvatar img,
.ctaFtThreadTextAvatar iframe,
.ctaFtThreadTextAvatar object,
.ctaFtThreadTextAvatar embed
{
	max-width: 100%;
}

.ctaFtThreadTextIcon img,
.ctaFtThreadTextIcon iframe,
.ctaFtThreadTextIcon object,
.ctaFtThreadTextIcon embed
{
	max-width: 100%;
}

.ctaFtThreadTextAvatarPage img,
.ctaFtThreadTextAvatarPage iframe,
.ctaFtThreadTextAvatarPage object,
.ctaFtThreadTextAvatarPage embed
{
	max-width: 100%;
}

.ctaFtThreadTextIconPage img,
.ctaFtThreadTextIconPage iframe,
.ctaFtThreadTextIconPage object,
.ctaFtThreadTextIconPage embed
{
	max-width: 100%;
}

/* --- likes_summary.css --- */

.likesSummary
{
	overflow: hidden; zoom: 1;
	font-size: 11px;
}

	.LikeText
	{
		float: left;
	}
	
	.likeInfo
	{
		float: right;
	}

/* --- login_bar.css --- */

/** Login bar basics **/

#loginBar
{
	color: rgb(203, 203, 203);
background-color: #032A46;
border-bottom: 1px solid rgb(87, 120, 142);
position: relative;
z-index: 1;

}

	#loginBar .ctrlWrapper
	{
		margin: 0 10px;
	}

	#loginBar .pageContent
	{
		padding-top: 5px;
		position: relative;
		_height: 0px;
	}

	#loginBar a
	{
		color: #6cb2e4;

	}

	#loginBar form
	{
		padding: 5px 0;
margin: 0 auto;
display: none;
line-height: 20px;
position: relative;

	}
	
		#loginBar .xenForm .ctrlUnit,		
		#loginBar .xenForm .ctrlUnit > dt label
		{
			margin: 0;
			border: none;
		}
	
		#loginBar .xenForm .ctrlUnit > dd
		{
			position: relative;
		}
	
	#loginBar .lostPassword,
	#loginBar .lostPasswordLogin
	{
		font-size: 11px;
	}
	
	#loginBar .rememberPassword
	{
		font-size: 11px;
	}

	#loginBar .textCtrl
	{
		color: rgb(245, 245, 245);
background-color: rgb(47, 68, 86);
border-color: rgb(87, 120, 142);

	}
	
	#loginBar .textCtrl[type=text]
	{
		font-weight: bold;
font-size: 18px;

	}

	#loginBar .textCtrl:-webkit-autofill /* http://code.google.com/p/chromium/issues/detail?id=1334#c35 */
	{
		background: rgb(47, 68, 86) !important;
		color: rgb(245, 245, 245);
	}

	#loginBar .textCtrl:focus
	{
		background: black none;

	}
	
	#loginBar input.textCtrl.disabled
	{
		color: rgb(203, 203, 203);
background-color: #032A46;
border-style: dashed;

	}
	
	#loginBar .button
	{
		min-width: 85px;
		*width: 85px;
	}
	
		#loginBar .button.primary
		{
			font-weight: bold;
		}
		
/** changes when eAuth is present **/

#loginBar form.eAuth
{
	-x-max-width: 700px; /* normal width + 170px */
}

	#loginBar form.eAuth .ctrlWrapper
	{
		border-right: 1px dotted #176093;
		margin-right: 200px;
		box-sizing: border-box;
	}

	#loginBar form.eAuth #eAuthUnit
	{
		position: absolute;
		top: 0px;
		right: 10px;
	}

		#eAuthUnit li
		{
			margin-top: 10px;
			line-height: 0;
		}
	
/** handle **/

#loginBar #loginBarHandle
{
	font-size: 11px;
color: rgb(245, 245, 245);
background-color: #032A46;
padding: 0 10px;
margin-right: 20px;
border-bottom-right-radius: 10px;
border-bottom-left-radius: 10px;
position: absolute;
right: 0px;
bottom: -20px;
text-align: center;
z-index: 1;
line-height: 20px;
box-shadow: 0px 2px 5px #032A46;

}


@media (max-width:800px)
{
	.Responsive #loginBar form.eAuth .ctrlWrapper
	{
		border-right: none;
		margin-right: 10px;
	}

	.Responsive #loginBar form.eAuth #eAuthUnit
	{
		position: static;
		width: 180px;
		margin: 0 auto 10px;
	}
}


/* --- message.css --- */



.messageList
{
	
}

.messageList .message
{
	background-color: rgb(240, 240, 240);
padding: 6px;
margin-top: 10px;
border: 1px solid rgb(150, 150, 150);

}

/* clearfix */ .messageList .message { zoom: 1; } .messageList .message:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

/*** Message block ***/

.message .messageInfo
{
	background-color: rgb(240, 240, 240);
padding: 0;
margin-left: 140px;
border-bottom: 1px none black;

	zoom: 1;
}

	.message .newIndicator
	{
		font-size: 11px;
color: #176093;
background: #6cb2e4 url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x top;
padding: 1px 5px;
margin: -5px -5px 5px 5px;
border: 1px solid #6cb2e4;
border-radius: 3px;
border-top-right-radius: 0px;
display: block;
float: right;
position: relative;
box-shadow: 1px 1px 3px rgba(0,0,0, 0.25);

		
		margin-right: -25px;
	}
	
		.message .newIndicator span
		{
			background-color: #6cb2e4;
border-top-right-radius: 3px;
position: absolute;
top: -4px;
right: -1px;
width: 5px;
height: 4px;

		}

	.message .messageContent
	{
		padding-bottom: 2px;
min-height: 100px;
overflow: hidden;
*zoom: 1;

	}
	
	.message .messageTextEndMarker
	{
		height: 0;
		font-size: 0;
		overflow: hidden;
	}
	
	.message .editDate
	{
		text-align: right;
		margin-top: 5px;
		font-size: 11px;
		color: rgb(100, 100, 100);
	}

	.message .signature
	{
		font-size: 9pt;
color: rgb(179, 0, 0);
padding: 5px 0 0;
margin-top: 5px;
border-top: 2px dashed rgb(203, 203, 203);

	}

	.message .messageMeta
	{
		font-size: 11px;
padding: 15px 5px 5px;
margin: -5px;
overflow: hidden;
zoom: 1;

	}

		.message .privateControls
		{
			float: left;

		}

		.message .publicControls
		{
			float: right;

		}
		
			.message .privateControls .item
			{
				margin-right: 10px;
				float: left;
			}

				.message .privateControls .item:last-child
				{
					margin-right: 0;
				}

			.message .publicControls .item
			{
				margin-left: 10px;
				float: left;
			}
	
				.message .messageMeta .control
				{
					
				}
				
					.message .messageMeta .control:focus
					{
						
					}
				
					.message .messageMeta .control:hover
					{
						
					}
				
					.message .messageMeta .control:active
					{
						
					}
	/*** multiquote +/- ***/
			
	.message .publicControls .MultiQuoteControl
	{
		padding-left: 4px;
		padding-right: 4px;
		border-radius: 2px;
		margin-left: 6px;
		margin-right: -4px;
	}
	
	
	.message .publicControls .MultiQuoteControl.active
	{
		background-color: rgb(237, 237, 237);
	}
	
		.messageNotices li
	{
		font-size: 11px;
background: #f9d9b0 url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/form-button-white-25px.png') repeat-x top;
padding: 5px;
margin: 10px 0;
border: 1px solid #f9d9b0;
border-radius: 5px;
line-height: 16px;

	}
	
		.messageNotices .icon
		{
			float: right;
			width: 16px;
			height: 16px;
			background: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/xenforo-ui-sprite.png') no-repeat 1000px 1000px;
		}
	
			.messageNotices .warningNotice .icon { background-position: -48px -32px; }		
			.messageNotices .deletedNotice .icon { background-position: -64px -32px; }		
			.messageNotices .moderatedNotice .icon {background-position: -32px -16px; }
	
	.message .likesSummary
	{
		padding: 5px;
margin-top: 10px;
border: 1px solid rgb(237, 237, 237);
border-radius: 5px;

	}
	
	.message .messageText > *:first-child
	{
		margin-top: 0;
	}

/* inline moderation changes */

.InlineModChecked .messageUserBlock,
.InlineModChecked .messageInfo,
.InlineModChecked .messageNotices,
.InlineModChecked .bbCodeBlock .type,
.InlineModChecked .bbCodeBlock blockquote,
.InlineModChecked .attachedFiles .attachedFilesHeader,
.InlineModChecked .attachedFiles .attachmentList
{
	background: rgb(255, 255, 200) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;

}

.InlineModChecked .messageUserBlock div.avatarHolder,
.InlineModChecked .messageUserBlock .extraUserInfo
{
	background: transparent;
}

.InlineModChecked .messageUserBlock .arrow span
{
	border-left-color: rgb(255, 255, 200);
}

/* message list */

.messageList .newMessagesNotice
{
	margin: 10px auto;
	padding: 5px 10px;
	border-radius: 5px;
	border: 1px solid rgb(203, 203, 203);
	background: rgb(237, 237, 237) url(/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/category-23px-light.png) repeat-x top;
	font-size: 11px;
}

/* deleted / ignored message placeholder */

.messageList .message.placeholder
{
}

.messageList .placeholder .placeholderContent
{	
	overflow: hidden; zoom: 1;
	color: rgb(87, 120, 142);
	font-size: 11px;
}

	.messageList .placeholder a.avatar
	{
		float: left;
		display: block;
	}
	
		.messageList .placeholder a.avatar img
		{
			display: block;
			width: 32px;
			height: 32px;
		}
		
	.messageList .placeholder .privateControls
	{
		margin-top: -5px;
	}
	

/* messages remaining link */

.postsRemaining a,
a.postsRemaining
{
	font-size: 11px;
	color: rgb(100, 100, 100);
}


@media (max-width:800px)
{
	.Responsive .message .newIndicator
	{
		margin-right: 0;
		border-top-right-radius: 3px;
	}
	
		.Responsive .message .newIndicator span
		{
			display: none;
		}
}

@media (max-width:480px)
{
	.Responsive .message .messageInfo
	{
		margin-left: 0;
		padding: 0 10px;
	}

	.Responsive .message .messageContent
	{
		min-height: 0;
	}	

	.Responsive .message .newIndicator
	{
		margin-right: -5px;
		margin-top: -16px;
	}

	.Responsive .message .postNumber,
	.Responsive .message .authorEnd
	{
		display: none;
	}
	
	.Responsive .message .signature
	{
		display: none;
	}
	
	.Responsive .messageList .placeholder a.avatar
	{
		margin-right: 10px;
	}
}


/* --- message_user_info.css --- */

.messageUserInfo
{
	float: left;
width: 124px;

}

	.messageUserBlock
	{
		background: rgb(237, 237, 237) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/tab-selected-light.png') repeat-x bottom;
border: 1px solid rgb(237, 237, 237);
border-radius: 5px;

		
		position: relative;
	}
		
		.messageUserBlock div.avatarHolder
		{
			background-color: rgb(245, 245, 245);
padding: 10px;
border-radius: 4px;

			
			position: relative;	
		}
		
			.messageUserBlock div.avatarHolder .avatar
			{
				display: block;
				font-size: 0;
			}
			
			.messageUserBlock div.avatarHolder .onlineMarker
			{
				position: absolute;
				top: 9px;
				left: 9px;
				
				border: 7px solid transparent;
border-top-color: rgb(127, 185, 0);
border-left-color: rgb(127, 185, 0);
border-top-left-radius: 5px;
border-top-right-radius: 3px;
border-bottom-left-radius: 3px;

			}
			
		.messageUserBlock h3.userText
		{
			padding: 6px;

		}
		
		.messageUserBlock .userBanner
		{
			display: block;
			margin-bottom: 5px;
			margin-left: -12px;
			margin-right: -12px;
		}
		
		.messageUserBlock .userBanner:last-child
		{
			margin-bottom: 0;
		}
	
		.messageUserBlock a.username
		{
			font-weight: bold;
font-size: 14px;
display: block;
overflow: hidden;
line-height: 16px;

			
		}
		
		.messageUserBlock .userTitle
		{
			font-size: 11px;
display: block;

		}
		
		.messageUserBlock .extraUserInfo
		{
			font-size: 10px;
background-color: rgb(245, 245, 245);
padding: 4px 6px;
border-radius: 4px;

		}
		
			.messageUserBlock .extraUserInfo dl
			{
				margin: 2px 0 0;
			}
							
			.messageUserBlock .extraUserInfo img
			{
				max-width: 100%;
			}
		
		.messageUserBlock .arrow
		{
			position: absolute;
			top: 10px;
			right: -10px;
			
			display: block;
			width: 0px;
			height: 0px;
			line-height: 0px;
			
			border: 10px solid transparent;
			border-left-color: rgb(237, 237, 237);
			-moz-border-left-colors: rgb(237, 237, 237);
			border-right: none;
			
			/* Hide from IE6 */
			_display: none;
		}
		
			.messageUserBlock .arrow span
			{
				position: absolute;
				top: -10px;
				left: -11px;
				
				display: block;
				width: 0px;
				height: 0px;
				line-height: 0px;
				
				border: 10px solid transparent;
				border-left-color: rgb(245, 245, 245);
				-moz-border-left-colors: rgb(245, 245, 245);
				border-right: none;
			}


@media (max-width:480px)
{
	.Responsive .messageUserInfo
	{
		float: none;
		width: auto; 
	}

	.Responsive .messageUserBlock
	{
		overflow: hidden;
		margin-bottom: 5px;
		position: relative;
	}

	.Responsive .messageUserBlock div.avatarHolder
	{
		float: left;
		padding: 5px;
	}

		.Responsive .messageUserBlock div.avatarHolder .avatar img
		{
			width: 48px;
			height: 48px;
		}
		
		.Responsive .messageUserBlock div.avatarHolder .onlineMarker
		{
			top: 4px;
			left: 4px;
			border-width: 6px;
		}

	.Responsive .messageUserBlock h3.userText
	{
		margin-left: 64px;
	}
	
	.Responsive .messageUserBlock .userBanner
	{
		max-width: 150px;
		margin-left: 0;
		margin-right: 0;
		border-top-left-radius: 3px;
		border-top-right-radius: 3px;
		position: static;
		display: inline-block;
	}
	
		.Responsive .messageUserBlock .userBanner span
		{
			display: none;
		}

	.Responsive .messageUserBlock .extraUserInfo
	{
		display: none;
	}

	.Responsive .messageUserBlock .arrow
	{
		display: none;
	}
}


/* --- notices.css --- */

.hasJs .FloatingContainer .Notice
{
	display: none;
}

.FloatingContainer
{
	position: fixed;
	width: 300px;
	z-index: 9997;
	top: auto;
	left: auto;
	bottom: 0;
	right: 20px;
}

.Notices .Notice .blockImage
{
	padding: 10px 0 5px 10px;
}

.Notices .Notice .blockImage,
.FloatingContainer .floatingImage
{
	float: left;
}

.Notices .Notice .blockImage img,
.FloatingContainer .floatingImage img
{
	max-width: 48px;
	max-height: 48px;
}

.Notices .hasImage,
.FloatingContainer .hasImage
{
	margin-left: 64px;
	min-height: 52px;
}

.FloatingContainer .floatingItem
{
	display: block;
	padding: 10px;
	font-size: 11px;
	position: relative;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 6px;
	box-shadow: 1px 1px 3px rgba(0,0,0, 0.25);
}

.FloatingContainer .floatingItem.primary
{
	background: rgb(245, 245, 245) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
	border-color: rgb(203, 203, 203);
}

.FloatingContainer .floatingItem.secondary
{
	color: #8f6c3f;
	background-color: #f9bc6d;
	border-color: #f9d9b0;
}

.FloatingContainer .floatingItem.dark
{
	color: #fff;
	background: black;
	background: url(/web/20190727050457im_/https://www.northeastshooters.com/xen/rgba.php?r=0&g=0&b=0&a=204); background: rgba(0,0,0, 0.8); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#CC000000,endColorstr=#CC000000);
	border-color: #333;
}

.FloatingContainer .floatingItem.light
{
	color: #000;
	background: white;
	background: url(/web/20190727050457im_/https://www.northeastshooters.com/xen/rgba.php?r=255&g=255&b=255&a=204); background: rgba(255,255,255, 0.8); _filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#CCFFFFFF,endColorstr=#CCFFFFFF);
	border-color: #ddd;
}

.FloatingContainer .floatingItem .title
{
	font-size: 14px;
	padding-bottom: 5px;
	font-weight: bold;
	display: block;
}

.FloatingContainer .floatingItem .DismissCtrl
{
	position: static;
	float: right;
	margin-left: 5px;
	margin-right: -5px;
	margin-top: -5px;
}

.Notices
{
	display: none;
}

/* cookie notice */
.Notice.notice_-1 .noticeCookiesContent .noticeCookiesContentRow
{
	text-align: center;
}

.Notice.notice_-1 .noticeCookiesContent .noticeCookiesContentRow.noticeCookiesContentButtonRow
{
	margin-top: 10px;
}


	@media (max-width:800px)
	{
		.Responsive .Notice.wide { display: none !important; }

		/* cookie notice */
		.Responsive .Notice.notice_-1 .noticeContent
		{
			padding: 3px 3px 10px 3px;
		}

		.Responsive .Notice.notice_-1 .noticeCookiesContent .noticeCookiesContentRow
		{
			font-size: 12px;
		}

		.Responsive .Notice.notice_-1 .noticeCookiesContent .noticeCookiesContentRow.noticeCookiesContentButtonRow .button
		{
			font-size: 11px;
			padding: 0px 4px;
			border-radius: 5px;
		}
	}
	
	@media (max-width:610px)
	{
		.Responsive .Notice.medium { display: none !important; }
	}
	
	@media (max-width:480px)
	{
		.Responsive .Notice.narrow { display: none !important; }
		
		.Responsive .FloatingContainer
		{
			right: 50%;
			margin-right: -150px;
		}
	}


/* --- panel_scroller.css --- */

.hasJs .Notices.PanelScroller { display: none; }

.PanelScroller .scrollContainer,
.PanelScrollerOff .panel
{
	background-color: rgb(220, 220, 220);
padding: 3px;
margin-bottom: 10px;
border: 1px solid rgb(87, 120, 142);
border-radius: 5px;
font-size: 13pt; color: white;

}

.PanelScroller .PanelContainer
{
	position: relative;
	clear: both;
	width: 100%;
	overflow: auto;
}

	.hasJs .PanelScroller .Panels
	{
		position: absolute;
	}

	.PanelScroller .Panels
	{
		clear: both;
		margin: 0;
		padding: 0;
	}
	
		.PanelScroller .panel,
		.PanelScrollerOff .panel
		{
			overflow: hidden;
			position: relative;
			padding: 0 !important;

			background-color: rgb(87, 120, 142);
padding: 10px;

		}
			
		.PanelScroller .panel .noticeContent,
		.PanelScrollerOff .panel .noticeContent
		{
			padding: 10px;
		}

/** panel scroller nav **/

.PanelScroller .navContainer
{
	margin: -11px 21px 10px;
overflow: hidden;
zoom: 1;

}

.PanelScroller .navControls
{
	float: right;
}

/* clearfix */ .PanelScroller .navControls { zoom: 1; } .PanelScroller .navControls:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

	.PanelScroller .navControls a
	{
		font-size: 9px;
background-color: rgb(245, 245, 245);
padding: 3px 6px 2px;
margin-left: -1px;
float: left;
display: block;
position: relative;

		
		border: 1px solid rgb(87, 120, 142);
border-radius: 5px;
		border-radius: 0;
	}
	
		.PanelScroller .navControls > a:first-child
		{
			border-bottom-left-radius: 5px;
		}
		
		.PanelScroller .navControls > a:last-child
		{
			border-bottom-right-radius: 5px;
		}
		
		.PanelScroller .navControls a:hover
		{
			text-decoration: none;
background-color: rgb(245, 245, 245);

		}
		
		.PanelScroller .navControls a.current
		{
			background-color: rgb(237, 237, 237);

		}
		
			.PanelScroller .navControls a .arrow
			{
				display: none;
			}
			
			.PanelScroller .navControls a.current span
			{
				display: block;
				line-height: 0px;
				width: 0px;
				height: 0px;
				border-top: 5px solid rgb(203, 203, 203);
				border-right: 5px solid transparent;
				border-bottom: 1px none black;
				border-left: 5px solid transparent;
				-moz-border-bottom-colors: rgb(203, 203, 203);
				position: absolute;
			}
			
			.PanelScroller .navControls a.current .arrow
			{
				border-top-color: rgb(87, 120, 142);
				top: 0px;
				left: 50%;
				margin-left: -5px;
			}
			
				.PanelScroller .navControls a .arrow span
				{
					border-top-color: rgb(220, 220, 220);
					top: -6px;
					left: -5px;
				}
				
/* notices */

.Notices .panel .noticeContent
{
	padding-right: 25px;
}

.bottomFixer .PanelScroller .panel,
.bottomFixer .PanelScrollerOff .panel
{
	margin-bottom: 0;
	border-radius: 0;
}

/* --- share_page.css --- */

.sharePage
{
}

/* clearfix */ .sharePage { zoom: 1; } .sharePage:after { content: '.'; display: block; height: 0; clear: both; visibility: hidden; }

	.sharePage .shareControl
	{
		float: left;
	}
	
	.sharePage .tweet
	{
		margin-right: 30px;
	}

	.sharePage .facebookLike .label
	{
		font-size: 11px;
		line-height: 24px;
		float: left;
		margin-right: 7px;
		display: none;
	}
	
	.sharePage iframe
	{
		height: 20px;
	}
	
	.sharePage .facebookLike iframe
	{
		z-index: 52;
	}
	



@media (max-width:480px)
{
	.Responsive .sharePage
	{
		display: none;
	}
}


/* --- thread_view.css --- */

.thread_view .threadAlerts
{
	border: 1px solid rgb(237, 237, 237);
	border-radius: 5px;
	font-size: 11px;
	margin: 10px 0;
	padding: 5px;
	line-height: 16px;
	background-image: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/form-button-white-25px.png');
}
	
	.thread_view .threadAlerts dt
	{
		color: #6cb2e4;
		display: inline;
	}
	
	.thread_view .threadAlerts dd
	{
		color: rgb(47, 68, 86);
		font-weight: bold;
		display: inline;
	}
	
		.thread_view .threadAlerts .icon
		{
			float: right;
			width: 16px;
			height: 16px;
			margin-left: 5px;
			background: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/xenforo-ui-sprite.png') no-repeat -1000px -1000px;
		}
		
			.thread_view .threadAlerts .deletedAlert .icon { background-position: -64px -32px; }
			.thread_view .threadAlerts .moderatedAlert .icon { background-position: -32px -16px; }
			.thread_view .threadAlerts .lockedAlert .icon { background-position: -16px -16px; }
	
.thread_view .threadAlerts + * > .messageList
{
	border-top: none;
}

.thread_view .threadNotices
{
	background-color: rgb(245, 245, 245);
	border: 1px solid rgb(203, 203, 203);
	border-radius: 5px;
	padding: 10px;
	margin: 10px auto;
}

.thread_view .InlineMod
{
	overflow: hidden; zoom: 1;
}

/* --- toggleme_auto.css --- */



.nodeList .CategoryStripCollapsed
{
	font-size: 11px;
color: rgb(0, 0, 0);
background: rgb(237, 234, 230) url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/category-23px-light.png') repeat-x center top;
padding: 5px 10px;
margin: 0;
border-top: 1px solid rgb(227, 227, 226);
border-bottom: 1px solid rgb(196, 196, 196);
border-top-left-radius: 5px;
border-top-right-radius: 5px;
border-bottom-right-radius: 5px;
border-bottom-left-radius: 5px;
min-height: 6px;

}

	.nodeList .CategoryStripCollapsed .nodeTitle
	{
		font-size: 10pt;
color: rgb(20,20,20);

	}
	
		.nodeList .CategoryStripCollapsed .nodeTitle a
		{
			color: rgb(20,20,20);
		}

	.nodeList .CategoryStripCollapsed .nodeDescription
	{
		font-style: italic;
font-size: 9pt;
color: rgb(20,20,20);

	}
	
		.nodeList .CategoryStripCollapsed .nodeDescription a
		{
			color: rgb(20,20,20);
		}
.nodeList .tglNodelist_forumview.active
{
	margin-bottom: 8px;
text-align: center;
cursor: pointer;

}
	.nodeList .tglNodelist_forumview span.toggleME_Collapse
	{
		font-size: 9pt;
color: #ffffff;
background-color: #176093;
padding: 2px 50px;
border: 1px solid #6cb2e4;
border-radius: 20px;
box-shadow: 0px 1px 2px rgba(0,0,0, 0.2);

	}
.nodeList .tglNodelist_forumview.inactive
{
	margin-bottom: 8px;
text-align: center;
cursor: pointer;

}
	.nodeList .tglNodelist_forumview span.toggleME_Expand
	{
		font-size: 9pt;
color: #ffffff;
background-color: #e68c17;
padding: 2px 50px;
border: 1px solid #f9bc6d;
border-radius: 20px;
box-shadow: 0px 1px 2px rgba(0,0,0, 0.2);

	}

.secondaryContentCollapsed
{
	background: #fff4e5 url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/gradients/category-23px-light.png') repeat-x top;
padding: 10px;
border-bottom: 1px solid #f9d9b0;

}

.secondaryContentCollapsed h3
{
	font-size: 12pt;
color: #f9bc6d;
padding-bottom: 2px;
margin-bottom: 5px;
border-bottom: 1px solid #f9d9b0;

}

.secondaryContentCollapsed h3 a:link, .secondaryContentCollapsed h3 a:visited
{
	text-decoration: none;
	color: #f9bc6d;
}

/* --- toggleme_manual.css --- */

/*Configure here button for categories with child*/
.tglWchild.inactive
{  
	float: right;
	display: inline;
	background-image: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png');
	background-position: 0px -16px;
	width: 16px;
	height: 16px;
	margin-right: 3px;
	cursor: pointer;
}	
.tglWchild.active
{
	float: right;
	display: inline;
	background-image: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png');
	background-position: 0px 0px;
	width: 16px;
	height: 16px;
	margin-right: 3px;
	cursor: pointer;
}

/*Configure here button for categories without child*/
.tglNOchild.inactive
{  
	float: right;
	display: inline;
	background-image: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png');
	background-position: 0px -16px;
	width: 16px;
	height: 16px;
	margin-right: 3px;
	margin-top: -5px;
	cursor: pointer;
}	
.tglNOchild.active
{
	float: right;
	display: inline;
	background-image: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png');
	background-position: 0px 0px;
	width: 16px;
	height: 16px;
	margin-right: 3px;
	margin-top: -5px;
	cursor: pointer;
}

/*Configure here buttons inside postbit */
.tglPosbit.pos_abvextra
{
	position: relative;
	bottom: 4px;
	right: -2px;
}

.extraUserInfo.toggleHidden
{
	display:none;
}

.tglPosbit.inactive
{  
	float: right;
	display: inline;
	background-image: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png');
	background-position: 0px -16px;
	width: 16px;
	height: 16px;
	cursor: pointer;
}	
.tglPosbit.active
{
	float: right;
	display: inline;
	background-image: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png');
	background-position: 0px 0px;
	width: 16px;
	height: 16px;
	cursor: pointer;
}

/*Configure here buttons for sidebar blocks */
.tglSidebar
{  
	float: right;
}

.tglSidebar.inactive
{  
	display:block;
	background: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png') 0px -16px;
	width: 16px;
	height: 16px;
	cursor: pointer;
}

.tglSidebar.active
{  
	display:block;
	background: url('/web/20190727050457im_/https://www.northeastshooters.com/xen/styles/default/xenforo/widgets/toggle-sprite.png') 0px 0px;
	width: 16px;
	height: 16px;
	cursor: pointer;
}

/* --- xcfw_copyright.css --- */

div.xcfw_copyright {
	clear:both;
	text-align:left;
	color: rgb(100,100,100);
}
				

/*
     FILE ARCHIVED ON 05:04:57 Jul 27, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 01:15:35 Nov 08, 2020.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  exclusion.robots.policy: 0.241
  RedisCDXSource: 8.522
  LoadShardBlock: 658.144 (3)
  exclusion.robots: 0.256
  load_resource: 56.144
  CDXLines.iter: 30.192 (3)
  PetaboxLoader3.datanode: 656.126 (4)
  PetaboxLoader3.resolve: 33.952
  captures_list: 701.511
  esindex: 0.019
*/